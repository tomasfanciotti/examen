<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Expo BA - Programadores </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="plugins/jquery-3.2.1.js"></script>
    <script src="plugins/jquery.validate.js"></script>
    <script src="plugins/additional-methods.js"></script>
    <script src="scripts.js"></script>


</head>
<body>
<div class="site-center-fullwidth" id="top" >
    <div class="site-center-maxwidth">
            <h3 id="menu">
               <a href="index.php">Inicio</a>
                |
                <a href="form.php">Postúlate</a>
                |
                <a href="http://www.buenosaires.gob.ar/">Buenos Aires Ciudad</a>
            </h3>
        <img src="resources/big_logo-buenos-aires-ciudad.png">
    </div>
</div>
<div id="cuerpo" class="site-center-fullwidth">
    <div class="site-center-maxwidth">
        <h1> EXPO BA - Jovenes programadores </h1>

        <div id="propuesta">
            <div id="p-text" class="rounded-left">
                <h3> ¡Esta es tu oportunidad!</h3>
                <p>Lorem ipsum dolor sit ta sanonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
            </div>
            <div id="p-img" class="rounded-right">
                <button type="button" class="btn btn-primary btn-lg"> Registrate aquí</button>
            </div>
        </div>
    </div>
</div>
<div class="site-center-fullwidth" id="footer"></div>
</body>
</html>