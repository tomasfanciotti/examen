<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Expo BA - Programadores </title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" href="ionicons.css" type="text/css">
    <!--
    <script src="plugins/jquery-3.2.1.js"></script>
    <script src="plugins/jquery.validate.js"></script>
    <script src="plugins/additional-methods.js"></script>

-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <script src="scripts.js"></script>

</head>
<body>
<div class="site-center-fullwidth" id="top" >
    <div class="site-center-maxwidth">
            <h3 id="menu">
               <a href="index.php">Inicio</a>
                |
                <a href="form.php">Postúlate</a>
                |
                <a href="http://www.buenosaires.gob.ar/">Buenos Aires Ciudad</a>
            </h3>
        <img src="resources/big_logo-buenos-aires-ciudad.png" onclick="prueba()">
    </div>
</div>
<div class="site-center-fullwidth">
    <div class="site-center-maxwidth">

        <form>

            <div class="my-4">
                <button id="prevbuttonForm" disabled="disabled" type="button" onclick="counter--;$('#dinamicForm').carousel('prev');if( counter === 0){ prevbutton.setAttribute('disabled','disabled')}" class="btn btn-secondary btn-lg">< Volver</button>
                <button id="nextbuttonForm" type="button" style="float: right" onclick="counter++;$('#dinamicForm').carousel('next'); if (counter !== 0) prevbutton.removeAttribute('disabled');" class="btn btn-primary btn-lg">Siguiente ></button>
                <script>
                    var counter = 0;
                    var prevbutton = document.getElementById('prevbuttonForm');
                </script>
            </div>

            <!-- Comienzo del formulario fisico-->

            <div id="dinamicForm" class="carousel slide" >
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="form-control py-4">
                            <h1>Datos personales</h1>
                            <hr/>
                            <div class="form-row my-4">

                                <div class="col form-group">
                                    <label for="name">Nombre</label>
                                    <input id="name" name="name" class="form-control" placeholder="Ingrese nombre..">
                                </div>
                                <div class="col form-group">
                                    <label for="surname">Apellido</label>
                                    <input id="surname" name="surname" class="form-control" placeholder="Ingrese apellido..">
                                </div>

                            </div>
                            <div class="form-row my-4">

                                <div class="col-3">
                                    <label for="gender">Género</label>
                                    <select id="gender" class="form-control">
                                        <option selected>Seleccione género</option>
                                        <option value="1">Hombre</option>
                                        <option value="2">Mujer</option>
                                        <option value="3">No es relevante</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label for="dni">DNI</label> <small class="text-muted">*</small>
                                    <input id="dni" name="dni" class="form-control" type="number" placeholder="Ingrese dni">
                                </div>
                                <div class="col form-group">
                                    <label for="estadocivil">Estado Civil</label>
                                    <select id="estadocivil" name="estadocivil" class="form-control">
                                        <option selected>Seleccione estado civil</option>
                                        <option value="1">soltero/a</option>
                                        <option value="2">Casado/a</option>
                                        <option value="3">Divorciado/a</option>
                                    </select>
                                </div>

                            </div>
                            <div class="form-row mt-4">

                                <div class="col form-group">
                                    <label for="numfij">N° telefono fijo</label> <small class="text-muted">*</small>
                                    <input id="numfij" name="numfij" class="form-control" type="number" placeholder="Ingrese telefono fijo">
                                </div>
                                <div class="col form-group">
                                    <label for="numcel">N° de  celular</label> <small class="text-muted">*</small>
                                    <input id="numcel" name="numcel" class="form-control" type="number" placeholder="Ingrese telefono celular">
                                </div>
                            </div>

                            <div class="form-group mt-3">
                                <label for="pais">Nacionalidad</label>
                                <select id="pais" name="pais" class="form-control col-5"></select>

                            </div>
                            <div class="form-group mt-3">
                                <label for="provincia">Provincia</label>
                                <select id="provincia" name="provincia" class="form-control col-5"></select>

                            </div>
                            <div class="form-group mt-3">
                                <label for="ciudad">Ciudad</label>
                                <select id="ciudad" name="ciudad" class="form-control col-5"></select>

                            </div>
                            <div class="form-group mt-3">
                                <label for="calle">Calle - Altura</label>
                                <div style="clear: both"></div>
                                <input id="calle" name="calle" class="form-control col-5" placeholder="Ingrese calle.." style="display: inline-block; float: left ;border-top-right-radius: 0;border-bottom-right-radius: 0">
                                <input id="calle-num" class="form-control col-2" type="number" placeholder="Ingrese número.." style="display: inline-block;border-left: none; border-bottom-left-radius: 0;border-top-left-radius: 0">
                                <div style="clear: both"></div>

                            </div>

                            <small class=" text-muted"> * Ingrese solo caracteres numericos [0-9]</small>

                        </div>
                    </div>

                    <!-- Cambio de seccion -->

                    <div class="carousel-item">


                        <div class="form-control my-4 py-4">
                            <h1>Objetivo laboral</h1>
                            <hr/>
                            <div class="form-group">

                                <label for="objetivo-labroal"></label>
                                <textarea id="objetivo-labroal" name="objetivo-labroal" placeholder="Escribe cual es tu objetivo laboral aquí" class="form-control" style="max-height: 100px"></textarea>

                            </div>
                        </div>
                    </div>


                    <!-- Cambio de seccion -->

                    <div class="carousel-item">
                        <div class="form-control my-4 py-4">

                            <h1>Experiencia laboral</h1>
                            <hr/>

                            <div id="sinExperiencias" style="text-align: center">

                                <h4 class="text-muted my-2" style="text-align: left">No hay cargada ninguna experiencia laboral</h4>

                            </div>
                            <div id="targetExperiencias">

                                <!--    ACA CAERAN TODOS LAS EXPERIENCIAS    -->


                            </div>
                            <button type="button" style="background-color: #3eb152" data-toggle="modal" data-target="#añadirExperiencia" class="form-control btn btn-success my-3"><b style="font-size: large">Añadir</b></button>

                        </div>
                    </div>

                    <!-- Cambio de seccion: ESTUDIOS-->

                    <div class="carousel-item">
                        <div class="form-control my-4 py-4">

                            <h1>Estudios</h1>
                            <hr/>

                            <div id="sinEstudios" style="display:inline-block; text-align: center">

                                <h4 class="text-muted my-2" style="text-align: left">No hay cargado ningun estudio</h4>

                            </div>
                            <div id="targetEstudios">

                                <!--    ACA CAERAN TODOS LOS ESTUDIOS  -->
                               
                                
                            </div>
                            <button type="button" style="background-color: #3eb152" data-toggle="modal" data-target="#añadirEstudio" class="form-control btn btn-success my-3"><b style="font-size: large">Añadir</b></button>

                        </div>
                    </div>

                    <!-- Cambio de seccion: IDIOMAS  -->

                    <div class="carousel-item">
                        <div class="form-control my-4 py-4">

                            <h1>Idiomas</h1>
                            <hr/>

                            <div id="sinIdiomas" style="display:inline-block; text-align: center">

                                <h4 class="text-muted my-2" style="text-align: left">No hay cargado ningun idioma</h4>

                            </div>
                            <div id="targetIdiomas">

                                <!--    ACA CAERAN TODOS LOS IDIOMAS  -->


                            </div>
                            <button type="button" style="background-color: #3eb152" data-toggle="modal" data-target="#añadirIdioma" class="form-control btn btn-success my-3"><b style="font-size: large">Añadir</b></button>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="site-center-fullwidth" id="footer"></div>

<!-- Modal de adicion de Experiencias Laborales -->

<div id="añadirExperiencia" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir experiencia laboral</h5>
                <button type="button" class="close" onclick="cancelarExperiencia()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                 <!-- cuerpo modal -->
                <form id="formModalExperiencia">
                    <div id="modalExperiencia">
                    <div class="form-row my-4">

                        <div class="col form-group">
                            <label for="modalExperiencia-empresa">Empresa</label>
                            <input id="modalExperiencia-empresa" class="form-control" placeholder="Ingrese nombre de la empresa..">
                        </div>
                        <div class="col form-group">
                            <label for="modalExperiencia-actividad">Actividad de la empresa</label>
                            <input id="modalExperiencia-actividad" class="form-control" placeholder="Ingrese actividad de la empresa..">
                        </div>

                    </div>
                    <div class="form-row my-4">

                        <div class="col-3">

                            <label for="modalExperiencia-puesto">Puesto</label>
                            <input id="modalExperiencia-puesto" class="form-control" placeholder="Puesto en la empresa..">

                        </div>
                        <div class="col form-group">

                            <label for="modalExperiencia-nivel">Nivel</label>
                            <input id="modalExperiencia-nivel" class="form-control" placeholder="Ingrese nivel del puesto..">

                        </div>
                        <div class="col form-group">

                            <label for="modalExperiencia-area">Área</label>
                            <input id="modalExperiencia-area" class="form-control" placeholder="Ingrese área en la empresa..">

                        </div>

                    </div>
                    <div class="form-group mt-3">

                        <label for="modalExperiencia-desde">Desde</label> - <label for="modalExperiencia-hasta">Hasta</label>
                        <div style="clear: both"></div>
                        <input id="modalExperiencia-desde" class="form-control" type="number" style="width: 150px;display: inline-block;float: left;border-top-right-radius: 0;border-bottom-right-radius: 0" placeholder="Año de alta..">
                        <div class="input-group-addon" style="display: inline-block;float: left">-</div>
                        <input id="modalExperiencia-hasta" class="form-control" type="number" style="width: 150px;display: inline-block;float: left;border-top-left-radius: 0;border-bottom-left-radius: 0" placeholder="Año de baja..">
                        <div style="clear: both"></div>

                    </div>
                    <div class="form-group mt-3">

                        <label for="modalExperiencia-acargo">Personas a cargo</label>
                        <select id="modalExperiencia-acargo" class="form-control col-5">
                            <option value="0">Ninguna</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">De 5 a 10</option>
                            <option value="6">Más de 10</option>
                        </select>

                    </div>
                    <div class="form-row my-4">
                        <div class="col">

                            <label for="modalExperiencia-referencia">Persona de referencia</label>
                            <input id="modalExperiencia-referencia" class="form-control" placeholder="Ingrese un referente..">

                        </div>
                        <div class="col">

                            <label for="modalExperiencia-vinculo">Vínculo</label>
                            <input id="modalExperiencia-vinculo" class="form-control" placeholder="Ingrese el vínculo..">


                        </div>
                        <div class="col">

                            <label for="modalExperiencia-contacto">Contacto</label>
                            <input id="modalExperiencia-contacto" class="form-control" placeholder="Ingrese un medio de contacto (ej.: correo)..">

                        </div>
                    </div>
                    <div class="form-group mt-3">

                        <label for="modalExperiencia-descripcion">Descripción</label>
                        <textarea id="modalExperiencia-descripcion" class="form-control" placeholder="Describa brevemente la experiencia laboral.." style="max-height: 100px"></textarea>

                    </div>
                    <div style="display: none">

                        <input id="modalExperiencia-id" >

                    </div>

                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="cancelarExperiencia()" class="btn btn-secondary" >Cancelar</button>
                <button type="button" onclick="crearExperiencia()"  class="btn btn-primary">Cargar experiencia</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal de adicion de Estudios -->

<div id="añadirEstudio" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir estudios</h5>
                <button type="button" class="close" onclick="cancelarEstudio()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <!-- cuerpo modal -->
                <form id="formModalEstudio">
                    <div id="modalEstudio">
                        <div class="form-row my-4">

                            <div class="col form-group">
                                <label for="modalEstudio-institucion">Casa de estudios</label>
                                <input id="modalEstudio-institucion" class="form-control" placeholder="Ingrese nombre de la institución..">
                            </div>
                            <div class="col form-group">
                                <label for="modalEstudio-titulo">Tíutulo</label>
                                <input id="modalEstudio-titulo" class="form-control" placeholder="Ingrese titulo o especialidad..">
                            </div>

                        </div>
                        <div class="form-group mt-3">

                            <label for="modalEstudio-desde">Desde</label> - <label for="modalEstudio-hasta">Hasta</label>
                            <div style="clear: both"></div>
                            <input id="modalEstudio-desde" class="form-control" type="number" style="width: 150px;display: inline-block;float: left;border-top-right-radius: 0;border-bottom-right-radius: 0" placeholder="Año de inicio..">
                            <div class="input-group-addon" style="display: inline-block;float: left">-</div>
                            <input id="modalEstudio-hasta" class="form-control" type="number" style="width: 150px;display: inline-block;float: left;border-top-left-radius: 0;border-bottom-left-radius: 0" placeholder="Año de egreso..">
                            <div style="clear: both"></div>

                        </div>
                        <div class="form-group mt-3">

                            <label for="modalEstudio-nivel">Nivel</label>
                            <select id="modalEstudio-nivel" class="form-control col-5">
								<option value="0">Seleccione nivel </option>
                                <option value="1">Secundario incompleto</option>
                                <option value="2">Secundario completo</option>
                                <option value="3">Terciario incompleto</option>
                                <option value="4">Terciario completo</option>
                                <option value="5">Universitario incompleto</option>
                                <option value="6">Universitario Completo</option>
                                <option value="7">Otro</option>
                            </select>

                        </div>
                        <div style="display: none">

                            <input id="modalEstudio-id" >

                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="cancelarEstudio()" class="btn btn-secondary" >Cancelar</button>
                <button type="button" onclick="crearEstudio()"  class="btn btn-primary">Cargar estudios</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal de adicion de Idiomas -->

<div id="añadirIdioma" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Añadir modal</h5>
                <button type="button" class="close" onclick="cancelarIdioma()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <!-- cuerpo modal -->
                <form id="formModalIdioma">
                    <div id="modalIdioma">
                        <div class="form-row my-4">

                            <div class="col-6 form-group">
                                <label for="modalIdioma-idioma">Idioma</label>
                                <input id="modalIdioma-idioma" class="form-control" placeholder="Ingrese el idioma..">
                            </div>
                            <div class="col">

                                <label for="modalIdioma-escrito">Escrito</label>
                                <select id="modalIdioma-escrito" class="form-control col-5">
                                    <option value="0">Seleccione nivel </option>
                                    <option value="1">Basico</option>
                                    <option value="2">Regular</option>
                                    <option value="3">Bueno</option>
                                    <option value="4">Excelente</option>
                                </select>

                            </div>
                            <div class="col">

                                <label for="modalIdioma-oral">Oral</label>
                                <select id="modalIdioma-oral" class="form-control col-5">
                                    <option value="0">Seleccione nivel </option>
                                    <option value="1">Basico</option>
                                    <option value="2">Regular</option>
                                    <option value="3">Bueno</option>
                                    <option value="4">Excelente</option>
                                </select>

                            </div>
                        </div>
                        <div style="display: none">

                            <input id="modalIdioma-id" >

                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="cancelarIdioma()" class="btn btn-secondary" >Cancelar</button>
                <button type="button" onclick="crearIdioma()"  class="btn btn-primary">Cargar idioma</button>
            </div>
        </div>
    </div>
</div>
<div id="modalBorrar" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Borrar registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Esta seguro que quiere eliminar este registro?</p>
                <input id="modalBorrar-id" style="display: none">
                <input id="modalBorrar-tipo" style="display: none">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="borrarRegistro()">Borrar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#dinamicForm').carousel({
            interval: false
        });

    });


    function deletar(id, idLinea) {

        $.ajax({
            url: "api/deletar.php",
            dataType: "json",
            data: { "id" : id },
            beforeSend: function( xhr ) {

                //xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
            }
        })
            .done(function( data ) {

                if(data.ok==true){
                    llamada_la_FUNCIN_SACA_LA_LINEA_DEHMLT();
                }
                console.log(data);
                alert(data.experiencias[0].name);

                /*
                if ( console && console.log ) {
                    console.log( "Sample of data:", data.slice( 0, 100 ) );
                }
                */
            });

    }

</script>
</body>
</html>