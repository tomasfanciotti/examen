var cantidad= {};

var counterExperiencias= 0;
cantidad["Experiencia"] = 0;

function crearExperiencia() {

    var names = ["Empresa", "Desde", "Hasta", "Descripcion", "Puesto", "actividad", "nivel", "area", "acargo", "referencia",
        "vinculo", "contacto", "id"];

    var inputs = {}, labels = {}, groups = {}, rows = {}, extras = {}, options = {};

    var idIndicador = document.getElementById("modalExperiencia-id");

    if (idIndicador.value === "") {

        idIndicador.value = counterExperiencias;

        var registroExperiencia = document.createElement('div');
        registroExperiencia.id = 'registroExperiencia[' + counterExperiencias + ']';
        registroExperiencia.className = 'form-control py-3 my-5';


        rows[1] = document.createElement('div');
        rows[1].className = 'form-row';

        rows[2] = document.createElement('div');
        rows[2].className = 'form-row';

        rows[3] = document.createElement('div');
        rows[3].style.display = 'none';

        rows[4] = document.createElement('div');
        rows[4].style.textAlign = 'right';


        for (var i = 0; i < names.length; i++) {

            labels[names[i]] = document.createElement('label');
            labels[names[i]].innerHTML = names[i];


            if (i === 3) {

                //La primera exepcion es porque la cuarta entrada es un "textarea" y no un input
                inputs[names[i]] = document.createElement('textarea');
                inputs[names[i]].id = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';
                inputs[names[i]].name = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';
                inputs[names[i]].setAttribute('style', 'max-height: 100px');


            } else if (i === 8) {

                //La segunda exepcion es porque la novena entrada es un "select" y no un input
                inputs[names[i]] = document.createElement('select');
                inputs[names[i]].id = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';
                inputs[names[i]].name = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';

                for (var a = 0; a < 7; a++) {

                    options[a] = document.createElement('option');
                    options[a].value = a;

                    switch (a) {

                        case 0:
                            options[a].innerHTML = 'Ninguna';
                            break;

                        case 5:
                            options[a].innerHTML = 'De 5 a 10';
                            break;

                        case 6:
                            options[a].innerHTML = 'Mas de 10';
                            break;

                        default:
                            options[a].innerHTML = a;
                            break;
                    }

                    inputs[names[i]].appendChild(options[a]);
                }


            } else {

                //Para cada una del resto de las entradas creo un "input"
                inputs[names[i]] = document.createElement('input');
                inputs[names[i]].id = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';
                inputs[names[i]].name = 'experiencia[' + counterExperiencias + '][' + names[i].toLowerCase() + ']';
            }

            if (i < 5) {

                //Solo las 5 primeras entradas se mostrarán, por lo tanto, las hago "solo-lectura" y asigno clases
                inputs[names[i]].setAttribute('readonly', 'readonly');
                inputs[names[i]].className = 'form-control';
                if (i !== 2) {

                    //Como la segunda entrada ("desde") y la tercera entrada ("hasta") van juntas,
                    // no necesito crear otro grupo para la tercera entrada.
                    groups[names[i]] = document.createElement('div');

                }

            } else {

                //Agrego las entradas ocultas a la tercera fila ( fila con display:none )
                rows[3].appendChild(labels[names[i]]);
                rows[3].appendChild(inputs[names[i]]);

            }
        }

        //Doy clase al grupo Empresa y agrego sus hijos.
        groups['Empresa'].className = 'form-group mr-5 col-7';
        groups['Empresa'].appendChild(labels['Empresa']);
        groups['Empresa'].appendChild(inputs['Empresa']);


        //Doy clase al grupo Desde (que incluye datos de "Hasta"), creo y agrego sus elementos hijos restantes.
        groups['Desde'].className = 'col form-group';

        extras['txt'] = document.createTextNode(' - ');

        extras['div1'] = document.createElement('div');
        extras['div1'].style.clear = 'both';

        extras['div2'] = document.createElement('div');
        extras['div2'].style.float = 'left';
        extras['div2'].style.display = 'inline-block';
        extras['div2'].className = 'input-group-addon';
        extras['div2'].innerHTML = '-';

        extras['div3'] = document.createElement('div');
        extras['div3'].style.clear = 'both';

        inputs['Desde'].style.width = '150px';
        inputs['Desde'].style.display = 'inline-block';
        inputs['Desde'].style.float = 'left';
        inputs['Desde'].style.borderTopRightRadius = "0";
        inputs['Desde'].style.borderBottomRightRadius = "0";

        inputs['Hasta'].style.width = '150px';
        inputs['Hasta'].style.display = 'inline-block';
        inputs['Hasta'].style.float = 'left';
        inputs['Hasta'].style.borderTopLeftRadius = "0";
        inputs['Hasta'].style.borderBottomLeftRadius = "0";

        groups['Desde'].appendChild(labels['Desde']);
        groups['Desde'].appendChild(extras['txt']);
        groups['Desde'].appendChild(labels['Hasta']);
        groups['Desde'].appendChild(extras['div1']);
        groups['Desde'].appendChild(inputs['Desde']);
        groups['Desde'].appendChild(extras['div2']);
        groups['Desde'].appendChild(inputs['Hasta']);
        groups['Desde'].appendChild(extras['div3']);


        //Agrego a los grupos 'Desde' y 'Empresa' como hijos de la primera fila.
        rows[1].appendChild(groups['Empresa']);
        rows[1].appendChild(groups['Desde']);


        //Doy clase al grupo Descripcion (que incluye datos de "Hasta") y agrego sus elementos hijos.
        groups['Descripcion'].className = 'col-7 mr-5 form-group';
        groups['Descripcion'].appendChild(labels['Descripcion']);
        groups['Descripcion'].appendChild(inputs['Descripcion']);

        //Doy clase al grupo Puesto (que incluye datos de "Hasta") y agrego sus elementos hijos.
        groups['Puesto'].className = 'col';
        groups['Puesto'].appendChild(labels['Puesto']);
        groups['Puesto'].appendChild(inputs['Puesto']);

        //Agrego a los grupos 'Descripcion' y 'Puesto' como hijos de la segunda fila.
        rows[2].appendChild(groups['Descripcion']);
        rows[2].appendChild(groups['Puesto']);

        //Creo los los elementos restantes (botones) y los agrego a la cuarta fila
        extras['button1'] = document.createElement('button');
        extras['button1'].type = 'button';
        extras['button1'].setAttribute("onclick", "borrarExperiencia(" + counterExperiencias + ")");
        extras['button1'].className = 'btn btn-secondary mr-3';
        extras['button1'].innerHTML = 'Borrar';

        extras['button2'] = document.createElement('button');
        extras['button2'].type = 'button';
        extras['button2'].setAttribute("onclick", "editarExperiencia(" + counterExperiencias + ")");
        extras['button2'].className = 'btn btn-primary mr-3';
        extras['button2'].innerHTML = 'Editar';

        rows[4].appendChild(extras['button1']);
        rows[4].appendChild(extras['button2']);

        //Por ultimo, agrego las filas a la div contenedora que se insertara en el targetExpereiencia
        registroExperiencia.appendChild(rows[1]);
        registroExperiencia.appendChild(rows[2]);
        registroExperiencia.appendChild(rows[3]);
        registroExperiencia.appendChild(rows[4]);

        //Aqui van las estilizaciones adicionales:
        inputs['Puesto'].style.width = '330.41px';

        //Asigno la ID de este registro.
        inputs['id'].value = counterExperiencias;

        document.getElementById('targetExperiencias').appendChild(registroExperiencia);
        counterExperiencias++;

        cantidad["Experiencia"]++;
        if (cantidad["Experiencia"] > 0){
            document.getElementById("sinExperiencias").style.display= "none";
        }
    }



    for (var e = 0; e < names.length; e++) {



            document.getElementById('experiencia[' + idIndicador.value + '][' + names[e].toLowerCase() + ']').value = document.getElementById("modalExperiencia-" + names[e].toLowerCase()).value;
          //  alert(document.getElementById('experiencia-' + idIndicador.value + '[' + names[e].toLowerCase() + ']').value);


    }


    $("#añadirExperiencia").modal('hide');
    document.getElementById("formModalExperiencia").reset();



}
function editarExperiencia(from) {

    var names = ["empresa", "desde", "hasta", "descripcion", "puesto", "actividad", "nivel", "area", "acargo", "referencia",
        "vinculo", "contacto", "id"];

    $("#añadirExperiencia").modal('show');

    for ( var a = 0; a < names.length ; a++){

        document.getElementById("modalExperiencia-"+names[a]).value= document.getElementById("experiencia[" + from + "][" + names[a] + "]").value;

    }

}
function borrarExperiencia(to){

    $("#modalBorrar").show();
    document.getElementById("modalBorrar-id").value = to;
    document.getElementById("modalBorrar-tipo").value = "Experiencia";

}
function cancelarExperiencia(){

    $("#añadirExperiencia").modal('hide');
    document.getElementById("formModalExperiencia").reset();
}

var counterEstudios= 0;
cantidad["Estudio"] = 0;

function crearEstudio() {

    var names = ["Institucion", "Desde", "Hasta", "Titulo", "Nivel", "id" ];
    var idIndicador= document.getElementById("modalEstudio-id");

	
    if (idIndicador.value === ""){
	
        idIndicador.value= counterEstudios;

        var rows= {}, groups = {}, labels = {}, frame = {}, h6s = {}, inputs = {} , extras = {}, options= {} ;

        //Creo el contenedor de los elementos generados
        var registroEstudio = document.createElement('div');
        registroEstudio.id = 'registroEstudio[' + counterEstudios + ']';
        registroEstudio.className = 'form-control py-3 my-5';

        //Creo las filas y les asigno clases
        rows[1]= document.createElement("div");
        rows[1].className= "form-row";

        rows[2]= document.createElement("div");
        rows[2].className= "form-row my-3";

        rows[3]= document.createElement("div");
        rows[3].style.display= "none";

        rows[4]= document.createElement("div");
        rows[4].className= "form-row";

        //Para cada elemento de Names
        for (var a = 0 ; a < names.length ; a++){

            //creo un grupo
            groups[names[a]]= document.createElement("div");

            //creo una label
            labels[names[a]]=document.createElement("label");
            labels[names[a]].innerHTML= names[a];

            //creo una div que voy a llamar frame
            frame[names[a]]=document.createElement("div");
            frame[names[a]].className= "form-control";

            //creo un elemento h6
            h6s[names[a]]= document.createElement("h6");
			h6s[names[a]].id= "h6Estudio["+counterEstudios+"]["+names[a].toLowerCase()+"]";
            h6s[names[a]].className= "text-secondary";

            //Pongo el elemento h6 dentro del frame
            frame[names[a]].appendChild(h6s[names[a]]);

            //Pongo el label y el frame dentro del grupo
            groups[names[a]].appendChild(labels[names[a]]);
            groups[names[a]].appendChild(frame[names[a]]);

            //Creo un input
            if( a !== 4 ){

                inputs[names[a]] = document.createElement("input");
                inputs[names[a]].id= "estudio[" + counterEstudios + "][" + names[a].toLowerCase() + "]";
                inputs[names[a]].name= "estudio[" + counterEstudios + "][" + names[a].toLowerCase() + "]";

            }else{

                //La segunda exepcion es porque la novena entrada es un "select" y no un input
                inputs[names[a]] = document.createElement('select');
                inputs[names[a]].id = 'estudio[' + counterEstudios + '][' + names[a].toLowerCase() + ']';
                inputs[names[a]].name = 'estudio[' + counterEstudios + '][' + names[a].toLowerCase() + ']';

                for (var e = 0; e < 8; e++) {

                    options[e] = document.createElement('option');
                    options[e].value = e;

                    switch (e) {

                        case 0:
                            options[e].innerHTML = 'Seleccione nivel';
                            break;

                        case 1:
                            options[e].innerHTML = 'Secundario incompleto';
                            break;

                        case 2:
                            options[e].innerHTML = 'Secundario completo';
                            break;

                        case 3:
                            options[e].innerHTML = 'Terciario incompleto';
                            break;

                        case 4:
                            options[e].innerHTML = 'Terciario completo';
                            break;

                        case 5:
                            options[e].innerHTML = 'Universitario incompleto';
                            break;

                        case 6:
                            options[e].innerHTML = 'Universitario completo';
                            break;


                        case 7:
                            options[e].innerHTML = 'Otro';
                            break;
                    }

                    inputs[names[a]].appendChild(options[e]);
                }


            }
            rows[3].appendChild(inputs[names[a]]);
        }

        groups["Institucion"].className= "col-9";
        groups["Desde"].className= "col";
        groups["Hasta"].className= "col";

        rows[1].appendChild(groups["Institucion"]);
        rows[1].appendChild(groups["Desde"]);
        rows[1].appendChild(groups["Hasta"]);

        groups["Titulo"].className= "col-7";
        groups["Nivel"].className= "col";
        rows[2].appendChild(groups["Titulo"]);
        rows[2].appendChild(groups["Nivel"]);

        //Creo los los elementos restantes (botones) y los agrego a la cuarta fila
        extras['button1'] = document.createElement('button');
        extras['button1'].type = 'button';
        extras['button1'].setAttribute("onclick", "borrarEstudio(" + counterEstudios + ")");
        extras['button1'].className = 'btn btn-secondary mr-3';
        extras['button1'].innerHTML = 'Borrar';

        extras['button2'] = document.createElement('button');
        extras['button2'].type = 'button';
        extras['button2'].setAttribute("onclick", "editarEstudio(" + counterEstudios + ")");
        extras['button2'].className = 'btn btn-primary mr-3';
        extras['button2'].innerHTML = 'Editar';

        rows[4].appendChild(extras['button1']);
        rows[4].appendChild(extras['button2']);

        registroEstudio.appendChild(rows[1]);
        registroEstudio.appendChild(rows[2]);
        registroEstudio.appendChild(rows[3]);
        registroEstudio.appendChild(rows[4]);
		
		inputs["id"].value= idIndicador;

        document.getElementById("targetEstudios").appendChild(registroEstudio);

        counterEstudios++;

        cantidad["Estudio"]++;
        if (cantidad["Estudio"] > 0){
            document.getElementById("sinEstudios").style.display= "none";
        }
    }

	
    for (var i = 0; i < names.length; i++) {

        document.getElementById('estudio[' + idIndicador.value + '][' + names[i].toLowerCase() + ']').value = document.getElementById("modalEstudio-" + names[i].toLowerCase()).value;
		
        if(i !== names.length-1 && i !== 4) {
			
			document.getElementById('h6Estudio[' + idIndicador.value + '][' + names[i].toLowerCase() + ']').innerHTML = document.getElementById("modalEstudio-" + names[i].toLowerCase()).value;
		
		}
		
		
		if( i === 4 ){

            switch(document.getElementById("modalEstudio-nivel").value) {

                case "0":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Seleccione nivel';
                    break;

                case "1":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Secundario incompleto';
                    break;

                case "2":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Secundario completo';
                    break;

                case "3":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML  = 'Terciario incompleto';
                    break;

                case "4":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Terciario completo';
                    break;

                case "5":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Universitario incompleto';
                    break;

                case "6":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Universitario completo';
                    break;


                case "7":
                    document.getElementById('h6Estudio[' + idIndicador.value + '][nivel]').innerHTML = 'Otro';
                    break;
            }
        }
	}


    $("#añadirEstudio").modal('hide');
    document.getElementById("formModalEstudio").reset();

}
function editarEstudio(from) {
	
	$("#añadirEstudio").modal('show');
	
	var names = ["institucion", "desde", "hasta", "titulo", "nivel", "id" ];
	
    for (var i = 0; i < names.length; i++) {

        document.getElementById("modalEstudio-" + names[i]).value = document.getElementById('estudio[' + from + '][' + names[i]+ ']').value;
		
    }
}
function borrarEstudio(to) {

    $("#modalBorrar").show();
    document.getElementById("modalBorrar-id").value = to;
    document.getElementById("modalBorrar-tipo").value = "Estudio";
}
function cancelarEstudio() {
    $("#añadirEstudio").modal('hide');
    document.getElementById("formModalEstudio").reset();
}

var counterIdiomas= 0;
cantidad["idioma"]= 0;

function crearIdioma(){


    var names = [ "Idioma", "Escrito"  , "Oral" , "id" ];

    var idIndicador= document.getElementById("modalIdioma-id");

    if( idIndicador.value === "" ){

        idIndicador.value = counterIdiomas;

        var rows = {}, groups = {}, labels = {} , h6s = {}, inputs = {}, extras = {}, options = {};

        var registroIdioma = document.createElement('div');
        registroIdioma.id = 'registroIdioma[' + counterIdiomas + ']';
        registroIdioma.className = 'form-control py-3 my-5';

        //Creo las filas y les asigno clases
        rows[1]= document.createElement("div");
        rows[1].className= "form-row";

        rows[2]= document.createElement("div");
        rows[2].style.display= "none";

        rows[3]= document.createElement("div");
        rows[3].className= "form-row";
        rows[3].style.textAlign= "right";

        for ( var a = 0 ; a < names.length ; a++){

            if (a < names.length-1) {
                //creo un grupo
                groups[names[a]] = document.createElement("div");

                //creo una label
                labels[names[a]] = document.createElement("label");
                labels[names[a]].innerHTML = names[a];

                //creo un elemento h6
                h6s[names[a]] = document.createElement("h6");
                h6s[names[a]].id = "h6Idioma[" + counterIdiomas + "][" + names[a].toLowerCase() + "]";
                h6s[names[a]].className = "text-secondary form-control";

                //Pongo el label y el h6 dentro del grupo
                groups[names[a]].appendChild(labels[names[a]]);
                groups[names[a]].appendChild(h6s[names[a]]);

                //Pongo el grupo dentro de la primera fila
                rows[1].appendChild(groups[names[a]]);
            }



            if ( a !== 1 || a!== 2 ){

                inputs[names[a]] = document.createElement("input");
                inputs[names[a]].id= "idioma[" + counterIdiomas + "][" + names[a].toLowerCase() + "]";
                inputs[names[a]].name= "idioma[" + counterIdiomas + "][" + names[a].toLowerCase() + "]";

            }else{

                //La primera y segunda exepcion es porque la segunda y tercera entrada ( "Oral" y "Escrito") son "select" y no "input"

                inputs[names[a]] = document.createElement('select');
                inputs[names[a]].id= "idioma[" + counterIdiomas + "][" + names[a].toLowerCase() + "]";
                inputs[names[a]].name= "idioma[" + counterIdiomas + "][" + names[a].toLowerCase() + "]";

                for (var e = 0; e < 5; e++) {

                    options[e] = document.createElement('option');
                    options[e].value = e;

                    switch (e) {

                        case 0:
                            options[e].innerHTML = 'Seleccione nivel';
                            break;

                        case 1:
                            options[e].innerHTML = 'Basico';
                            break;

                        case 2:
                            options[e].innerHTML = 'Regular';
                            break;

                        case 3:
                            options[e].innerHTML = 'Bueno';
                            break;

                        case 4:
                            options[e].innerHTML = 'Exelente';
                            break;
                    }

                    inputs[names[a]].appendChild(options[e]);
                }
            }

            rows[2].appendChild(inputs[names[a]]);

        }

        inputs["id"].value= idIndicador;


        groups["Idioma"].className= "col-6";
        groups["Escrito"].className= "col";
        groups["Oral"].className= "col";

        //Creo los los elementos restantes (botones) y los agrego a la tercera fila
        extras['button1'] = document.createElement('button');
        extras['button1'].type = 'button';
        extras['button1'].setAttribute("onclick", "borrarIdioma(" + counterIdiomas + ")");
        extras['button1'].className = 'btn btn-secondary mr-3';
        extras['button1'].innerHTML = 'Borrar';

        extras['button2'] = document.createElement('button');
        extras['button2'].type = 'button';
        extras['button2'].setAttribute("onclick", "editarIdioma(" + counterIdiomas + ")");
        extras['button2'].className = 'btn btn-primary mr-3';
        extras['button2'].innerHTML = 'Editar';

        rows[3].appendChild(extras['button1']);
        rows[3].appendChild(extras['button2']);

        registroIdioma.appendChild(rows[1]);
        registroIdioma.appendChild(rows[2]);
        registroIdioma.appendChild(rows[3]);

        document.getElementById("targetIdiomas").appendChild(registroIdioma);

        counterIdiomas++;
        cantidad["idiomas"]++;

        if (cantidad["idiomas"] > 0){
            document.getElementById("sinIdiomas").style.display= "none";
        }
    }

    for (var i = 0; i < names.length; i++) {

        document.getElementById('idioma[' + idIndicador.value + '][' + names[i].toLowerCase() + ']').value = document.getElementById("modalIdioma-" + names[i].toLowerCase()).value;

        if( i === 0) {

            document.getElementById('h6Idioma[' + idIndicador.value + '][' + names[i].toLowerCase() + ']').innerHTML = document.getElementById("modalIdioma-" + names[i].toLowerCase()).value;

        }


        if( i === 1 ||  i === 2 ){

            switch(document.getElementById("modalIdioma-"+names[i].toLowerCase()).value) {

                case "0":
                    document.getElementById('h6Idioma[' + idIndicador.value + ']['+names[i].toLowerCase()+']').innerHTML = 'Seleccione nivel';
                    break;

                case "1":
                    document.getElementById('h6Idioma[' + idIndicador.value + ']['+names[i].toLowerCase()+']').innerHTML = 'Básico';
                    break;

                case "2":
                    document.getElementById('h6Idioma[' + idIndicador.value + ']['+names[i].toLowerCase()+']').innerHTML = 'Regular';
                    break;

                case "3":
                    document.getElementById('h6Idioma[' + idIndicador.value + ']['+names[i].toLowerCase()+']').innerHTML  = 'Bueno';
                    break;

                case "4":
                    document.getElementById('h6Idioma[' + idIndicador.value + ']['+names[i].toLowerCase()+']').innerHTML = 'Exelente';
                    break;
            }
        }
    }


    $("#añadirIdioma").modal('hide');
    document.getElementById("formModalIdioma").reset();



}
function editarIdioma(from){

    $("#añadirIdioma").modal('show');

    var names = [ "idioma", "escrito"  , "oral" , "id" ];

    for (var i = 0; i < names.length; i++) {

        document.getElementById("modalIdioma-" + names[i]).value = document.getElementById('idioma[' + from + '][' + names[i]+ ']').value;

    }
}
function borrarIdioma(to){

    $("#modalBorrar").show();
    document.getElementById("modalBorrar-id").value = to;
    document.getElementById("modalBorrar-tipo").value = "Idioma";

}
function cancelarIdioma(){
    $("#añadirIdioma").modal('hide');
    document.getElementById("formModalIdioma").reset();
}


function borrarRegistro(){

    var tipo =  document.getElementById("modalBorrar-tipo").value;
    var id = document.getElementById("modalBorrar-id").value;

    document.getElementById("registro"+ tipo +"[" + id + "]").style.display = "none";
      
    cantidad[tipo]--;

    if (cantidad[tipo] === 0 ) {
         document.getElementById("sin"+ tipo +"s").style.display = "inline-block";
    }

    $("#modalBorrar").hide();
 }